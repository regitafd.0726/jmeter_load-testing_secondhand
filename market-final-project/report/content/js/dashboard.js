/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 97.33333333333333, "KoPercent": 2.6666666666666665};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.8766666666666667, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [1.0, 500, 1500, "Post Buyer Order"], "isController": false}, {"data": [0.55, 500, 1500, "Post Register"], "isController": false}, {"data": [1.0, 500, 1500, "Delete Seller Product ID"], "isController": false}, {"data": [1.0, 500, 1500, "Get Buyer Order"], "isController": false}, {"data": [0.725, 500, 1500, "Post Seller Product"], "isController": false}, {"data": [1.0, 500, 1500, "Put Buyer Order ID"], "isController": false}, {"data": [0.95, 500, 1500, "Get Buyer Product"], "isController": false}, {"data": [1.0, 500, 1500, "Get Buyer Product ID"], "isController": false}, {"data": [0.95, 500, 1500, "Get Seller Product ID"], "isController": false}, {"data": [1.0, 500, 1500, "Get Buyer Order ID"], "isController": false}, {"data": [0.85, 500, 1500, "Post Login"], "isController": false}, {"data": [1.0, 500, 1500, "Get Seller Product"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 150, 4, 2.6666666666666665, 455.9333333333335, 256, 1835, 300.0, 860.2, 1572.1499999999996, 1802.8700000000006, 17.75568181818182, 12.447472774621211, 119.26188613429214], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["Post Buyer Order", 10, 0, 0.0, 303.5, 271, 364, 289.0, 362.2, 364.0, 364.0, 4.464285714285714, 2.942766462053571, 2.3367745535714284], "isController": false}, {"data": ["Post Register", 20, 4, 20.0, 1007.3000000000003, 347, 1835, 872.5, 1772.0, 1831.85, 1835.0, 3.094059405940594, 1.7736453821163365, 3.2202088006652225], "isController": false}, {"data": ["Delete Seller Product ID", 10, 0, 0.0, 286.5, 268, 320, 282.0, 318.6, 320.0, 320.0, 4.231908590774439, 1.3307368810833686, 2.0663616165890817], "isController": false}, {"data": ["Get Buyer Order", 10, 0, 0.0, 275.6, 263, 300, 275.0, 297.9, 300.0, 300.0, 4.568296025582457, 5.308859639104614, 2.9042585084513477], "isController": false}, {"data": ["Post Seller Product", 20, 0, 0.0, 612.4, 276, 1624, 572.5, 898.0000000000001, 1587.8499999999995, 1624.0, 4.943153732081068, 3.2608401816608996, 229.38115963297082], "isController": false}, {"data": ["Put Buyer Order ID", 10, 0, 0.0, 278.6, 267, 289, 280.5, 288.5, 289.0, 289.0, 4.710315591144607, 3.0819447715496935, 2.3413580428638716], "isController": false}, {"data": ["Get Buyer Product", 10, 0, 0.0, 393.8, 288, 655, 347.5, 638.7, 655.0, 655.0, 4.299226139294927, 3.182434974204643, 2.204193088993981], "isController": false}, {"data": ["Get Buyer Product ID", 10, 0, 0.0, 265.0, 256, 290, 261.0, 288.3, 290.0, 290.0, 4.4424700133274095, 3.7743641714793426, 2.0780694691248334], "isController": false}, {"data": ["Get Seller Product ID", 10, 0, 0.0, 318.1, 267, 637, 281.5, 603.3000000000002, 637.0, 637.0, 4.286326618088299, 3.0096375375053577, 2.854760501500214], "isController": false}, {"data": ["Get Buyer Order ID", 10, 0, 0.0, 281.5, 259, 340, 281.0, 334.6, 340.0, 340.0, 4.570383912248629, 5.3023594606946975, 2.062028679159049], "isController": false}, {"data": ["Post Login", 20, 0, 0.0, 440.2999999999999, 332, 653, 407.0, 594.8000000000001, 650.3, 653.0, 3.817522427944264, 2.106347824012216, 1.8509764506585227], "isController": false}, {"data": ["Get Seller Product", 10, 0, 0.0, 316.40000000000003, 278, 458, 302.0, 446.20000000000005, 458.0, 458.0, 5.037783375314861, 3.547111146095718, 3.2814467884130982], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["The operation lasted too long: It took 1,764 milliseconds, but should not have lasted longer than 1,700 milliseconds.", 1, 25.0, 0.6666666666666666], "isController": false}, {"data": ["The operation lasted too long: It took 1,772 milliseconds, but should not have lasted longer than 1,700 milliseconds.", 2, 50.0, 1.3333333333333333], "isController": false}, {"data": ["The operation lasted too long: It took 1,835 milliseconds, but should not have lasted longer than 1,700 milliseconds.", 1, 25.0, 0.6666666666666666], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 150, 4, "The operation lasted too long: It took 1,772 milliseconds, but should not have lasted longer than 1,700 milliseconds.", 2, "The operation lasted too long: It took 1,764 milliseconds, but should not have lasted longer than 1,700 milliseconds.", 1, "The operation lasted too long: It took 1,835 milliseconds, but should not have lasted longer than 1,700 milliseconds.", 1, "", "", "", ""], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": ["Post Register", 20, 4, "The operation lasted too long: It took 1,772 milliseconds, but should not have lasted longer than 1,700 milliseconds.", 2, "The operation lasted too long: It took 1,764 milliseconds, but should not have lasted longer than 1,700 milliseconds.", 1, "The operation lasted too long: It took 1,835 milliseconds, but should not have lasted longer than 1,700 milliseconds.", 1, "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
