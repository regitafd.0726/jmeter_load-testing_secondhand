## Project Name
JMeter Performance Testing Secondhand Web

## Project Description
Performed performance testing on secondhand websites based on a test plan with several scenarios for each API. Configured JMeter for performance testing with 10 number of threads, 1 second ramp-up period, and 1 count loop. Created a report in .jtl format and generate .jtl report in .html format
